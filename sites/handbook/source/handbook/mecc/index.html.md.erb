---
layout: handbook-page-toc
title: "Managing so Everyone Can Contribute (MECC)"
canonical_path: "/handbook/mecc/"
description: GitLab Management Philosophy — Managing so Everyone Can Contribute (MECC)
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

{::options parse_block_html="true" /}

<%= partial("handbook/mecc/_mecc_overview.erb") %>

## MECC Vision

It is GitLab's [mission](/company/mission/) to make it so that **everyone can contribute**. When applied to management, this creates an atmosphere where everyone is empowered to lead. 

MECC differentiates itself from other management philosophies by consciously enabling decentralized decision making at a centralized (organizational) level. While guiding principles exist, it is not static. It is designed to be iterated on and evolved by everyone. The underpinnings of its philosophy are designed to apply to **all** work environments, from [no remote to strictly remote](/company/culture/all-remote/stages/). 

By implementing MECC's unique foundational principles at an *organizational* level, *individuals* within the organization are less constrained. Each team member receives greater agency to exert self-leadership. Collectively, we believe this atmosphere allows for more informed decisions, made quicker, more frequently, and with a higher likelihood of successful execution. 

MECC is a recipe which has worked at GitLab. It may not be perfectly applicable in your company, and that's OK. As with [The Remote Playbook](https://learn.gitlab.com/allremote/), we are transparently sharing it to inspire other organizations and to invite conversation. 

## Understanding and applying MECC

1. **MECC describes an ideal state**. In management, it is not possible to remain in an ideal state in perpetuity. Competing priorities, conflict tradeoffs, and [coordination headwinds](https://komoroske.com/slime-mold/) will be present at varying times. When applying MECC, resist the urge to take a binary approach. Rather than asking, "Have we completely achieved MECC in our team or company?," leverage MECC principles to *navigate* through the aforementioned priorities, tradeoffs, and headwinds with [more information](/handbook/mecc/informed-decisions/) and [greater velocity](/handbook/mecc/fast-decisions/). 
1. **MECC is for individual contributors *and* people managers.** The term "Managing" in "Managing so Everyone Can Contribute" is not to be conflated with the term "People Manager." Every contributor, including an individual contributor, is a [Manager of One](/handbook/leadership/#managers-of-one). MECC empowers individual contributors to be better stewards of their own time and attention. MECC empowers people managers to lead with deeper conviction while creating more space for their direct reports to grow, develop, and contribute.

## Prerequisites for MECC

There are a number of foundational elements that should be in place in order for MECC to be most successful within a team or organization. These prerequisites consist of the processes, organizational structure, and culture that create an *ideal* environment to implement MECC principles. 

If your organization is missing some of these building blocks, consider this an opportunity to invest in your team. [GitLab's Remote Playbook](https://learn.gitlab.com/suddenlyremote) can serve as a blueprint. 

1. **Communication guidelines.** You'll want to have [a robust list of guardrails and tips](/handbook/communication/) outlining *all* aspects of communication within the organization. This includes how to approach sensitive topics, what tools to use for various types of interactions, and how to [embrace asynchronous communication](/company/culture/all-remote/asynchronous/). There should be [no unwritten rules](/company/culture/all-remote/building-culture/#no-unwritten-rules-in-a-remote-work-culture).
1. **Shared set of values.** Your [core values](/handbook/values/) must be more than words on a page. They should be actionable, clearly documented, and [reinforced in](/company/culture/all-remote/building-culture/#reinforcing-your-values) everything you do as a team. These values also act as a [filter for hiring](/company/culture/all-remote/building-culture/#how-do-i-assess-culture-fit-remotely), ensuring you continue to grow the team with people who are committed to living out these values in their work.
1. **Team trust.** Implementing new management techniques can be uncomfortable at first. A baseline of [trust](/handbook/leadership/building-trust/) across the organization will better enable the team to embrace change and assume positive intent along the way. 
1. **Focus on results.** [Measuring output](/company/culture/all-remote/management/#focusing-on-results) instead of input is foundational to managing a distributed team. This means establishing clear, transparent goals so that team members at all levels of the organization can see and take ownership for how their work is contributing to the team, department, and company's success.
1. **Culture of belonging.** Prioritize cultivating an [inclusive](/company/culture/inclusion/) environment where team members feel a sense of belonging and [psychological safety](/handbook/leadership/emotional-intelligence/psychological-safety/). This unlocks the potential of your team and creates a [non-judgmental culture](/company/culture/all-remote/mental-health/#create-a-non-judgemental-culture) that welcomes diverse contributions and ideas.

## MECC Certification

In Q3-FY23, we plan to release a 🎓 MECC Certification course 🎓, free and open to the public. To be amongst the first to earn the certification, sign up below and we will email you when it's available.

<iframe class="mj-w-res-iframe" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://app.mailjet.com/widget/iframe/6wMz/NG1" width="100%"></iframe>

<script type="text/javascript" src="https://app.mailjet.com/statics/js/iframeResizer.min.js"></script>
